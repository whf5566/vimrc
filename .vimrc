 
""""""""""""""""""""""""""""vim基础配置"""""""""""""""""""""""""""""
" kj 替换 Esc
inoremap kj <Esc>
vnoremap kj <Esc> 
" 修改leader键
let mapleader = ','
let g:mapleader = ','
" 语法高亮
syntax on
" 针对不同的文件类型采用不同的缩进格式
filetype indent on
" 允许插件
filetype plugin on
" 允许自动补全
filetype plugin indent on

set autoread    " 文件修改之后自动载入
" 去掉输入错误的提示声音
set novisualbell
set noerrorbells
set t_vb=
set tm=800

set laststatus=2

set history=700
set nocompatible	" 配置兼容 
set backspace=2		
" tab键相关设置
set tabstop=4 "Tab键的宽度
set softtabstop=4 " 按退格键时可以一次删掉多少空格
set shiftwidth=4 " 每一次缩进多少空格
set expandtab " 键Tab自动转为空格
set smarttab
" 缩进配置
set smartindent
set autoindent
""在状态栏显示正在输入的命令
set showcmd
" 左下角显示当前vim模式
set showmode

" 在上下移动光标时，光标的上方或下方至少会保留显示的行数
set scrolloff=7

set number
set ruler
set ignorecase
set hls
set helplang=cn

autocmd! bufwritepost .vimrc source % " vimrc文件修改之后自动加载。 linux


" 相对行号      行号变成相对，可以用 nj  nk   进行跳转 5j   5k 上下跳5行
set relativenumber number
au FocusLost * :set norelativenumber number
au FocusGained * :set relativenumber
" 插入模式下用绝对行号, 普通模式下用相对
autocmd InsertEnter * :set norelativenumber number
autocmd InsertLeave * :set relativenumber
function! NumberToggle()
    if(&relativenumber == 1)
        set norelativenumber number
    else
        set relativenumber
    endif
endfunc
nnoremap <C-n> :call NumberToggle()<cr>


"==========================================
" FileEncode Settings 文件编码,格式
"==========================================
" 设置新文件的编码为 UTF-8
set encoding=utf-8
" 自动判断编码时，依次尝试以下编码：
set fileencodings=ucs-bom,utf-8,cp936,gb18030,big5,euc-jp,euc-kr,latin1
set helplang=cn
"set langmenu=zh_CN.UTF-8
"set enc=2byte-gb18030
" 下面这句只影响普通模式 (非图形界面) 下的 Vim。
set termencoding=utf-8

" Use Unix as the standard file type
set ffs=unix,dos,mac

" 如遇Unicode值大于255的文本，不必等到空格再折行。
set formatoptions+=m
" 合并两行中文时，不在中间加空格：
set formatoptions+=B

"==========================================
" HotKey Settings  自定义快捷键设置
"==========================================

"为方便复制，用<F2>开启/关闭行号显示:
function! HideNumber()
    if(&relativenumber == &number)
        set relativenumber! number!
    elseif(&number)
        set number!
    else
        set relativenumber!
    endif
    set number?
endfunc
nnoremap <F2> :call HideNumber()<CR>

"Smart way to move between windows 分屏窗口移动
"map <C-j> <C-W>j
"map <C-k> <C-W>k
"map <C-h> <C-W>h
"map <C-l> <C-W>l

" Go to home and end using capitalized directions
noremap H ^
noremap L $


"Map ; to : and save a million keystrokes
" ex mode commands made easy 用于快速进入命令行
nnoremap ; :

" tab 操作
" TODO: ctrl + n 变成切换tab的方法
" http://vim.wikia.com/wiki/Alternative_tab_navigation
" http://stackoverflow.com/questions/2005214/switching-to-a-particular-tab-in-vim
"map <C-2> 2gt
map <leader>th :tabfirst<cr>
map <leader>tl :tablast<cr>

map <leader>tj :tabnext<cr>
map <leader>tk :tabprev<cr>
map <leader>tn :tabnext<cr>
map <leader>tp :tabprev<cr>

map <leader>te :tabedit<cr>
map <leader>td :tabclose<cr>
map <leader>tm :tabm<cr>


" 新建tab  Ctrl+t
nnoremap <C-t>     :tabnew<CR>
inoremap <C-t>     <Esc>:tabnew<CR>
" TODO: 配置成功这里, 切换更方便, 两个键
" nnoremap <C-S-tab> :tabprevious<CR>
" nnoremap <C-tab>   :tabnext<CR>
" inoremap <C-S-tab> <Esc>:tabprevious<CR>i
" inoremap <C-tab>   <Esc>:tabnext<CR>i
" nnoremap <C-Left> :tabprevious<CR>
" nnoremap <C-Right> :tabnext<CR>

" normal模式下切换到确切的tab
noremap <leader>1 1gt
noremap <leader>2 2gt
noremap <leader>3 3gt
noremap <leader>4 4gt
noremap <leader>5 5gt
noremap <leader>6 6gt
noremap <leader>7 7gt
noremap <leader>8 8gt
noremap <leader>9 9gt
noremap <leader>0 :tablast<cr>

" Toggles between the active and last active tab "
" The first tab is always 1 "
let g:last_active_tab = 1
" nnoremap <leader>gt :execute 'tabnext ' . g:last_active_tab<cr>
nnoremap <silent> <c-l> :execute 'tabnext ' . g:last_active_tab<cr>
vnoremap <silent> <c-l> :execute 'tabnext ' . g:last_active_tab<cr>
autocmd TabLeave * let g:last_active_tab = tabpagenr()


"==========================================
" Theme Settings  主题设置
"==========================================

" theme主题
"set background=dark
"colorscheme solarized
set t_Co=256

"colorscheme molokai
"colorscheme desert

"设置标记一列的背景颜色和数字一行颜色一致
hi! link SignColumn LineNr 
hi! link ShowMarksHLl DiffAdd
hi! link ShowMarksHLu DiffChange

"" for error
"highlight，防止错误整行标红导致看不清
highlight clear SpellBad
highlight SpellBad term=standout ctermfg=1 term=underline cterm=underline
highlight clear SpellCap 
highlight SpellCap term=underline cterm=underline
highlight clear SpellRare
highlight SpellRare term=underline cterm=underline
highlight clear SpellLocal
highlight SpellLocal term=underline cterm=underline




"""""""""""""""""""""""""""vim插件配置""""""""""""""""""""""""""""
set nocompatible               " be iMproved
filetype off                   " required!
filetype plugin indent on      " required!
set rtp+=~/.vim/bundle/vundle/
call vundle#rc()
Bundle 'gmarik/vundle'
"################### 显示增强-主题 ###################"

"主题 solarized
Bundle 'altercation/vim-colors-solarized'
"let g:solarized_termcolors=256
let g:solarized_termtrans=1
let g:solarized_contrast="normal"
let g:solarized_visibility="normal"

"主题 molokai
Bundle 'tomasr/molokai'
"let g:molokai_original = 1


"################### 自动补全 ###################

" 代码自动补全
Bundle 'Valloric/YouCompleteMe'
"youcompleteme  默认tab  s-tab 和自动补全冲突
"let g:ycm_key_list_select_completion=['<c-n>']
let g:ycm_key_list_select_completion = ['<Down>']
"let g:ycm_key_list_previous_completion=['<c-p>']
let g:ycm_key_list_previous_completion = ['<Up>']
let g:ycm_complete_in_comments = 1  "在注释输入中也能补全
let g:ycm_complete_in_strings = 1   "在字符串输入中也能补全
let g:ycm_use_ultisnips_completer = 1 "提示UltiSnips
let g:ycm_collect_identifiers_from_comments_and_strings = 1
"注释和字符串中的文字也会被收入补全
let g:ycm_collect_identifiers_from_tags_files = 1

" 跳转到定义处, 分屏打开
let g:ycm_goto_buffer_command = 'horizontal-split'
nnoremap <leader>jd :YcmCompleter GoToDefinition<CR>
nnoremap <leader>gd :YcmCompleter GoToDeclaration<CR>

"""""""""""""""""""""""""""winmanager配置"""""""""""""""""""""""""
Bundle 'winmanager'
let g:winManagerWindowLayout = "FileExplorer|TagList"
let g:winManagerWidth = 30
let g:defaultExplorer = 0
nmap <C-W><C-F> :FirstExplorerWindow<cr>
nmap <C-W><C-B> :BottomExplorerWindow<cr>
nmap wm :WMToggle<cr>


"""""""""""""""""""""""""""""gitgutter配置"""""""""""""""""""""
Bundle 'airblade/vim-gitgutter'
let g:gitgutter_enabled = 0
nmap <Leader>git :GitGutterToggle<cr>
nmap ]h <Plug>GitGutterNextHunk
nmap [h <Plug>GitGutterPrevHunk



""""""""""""""""""""""""""""""""taglist配置""""""""""""""""""""""
Bundle 'taglist.vim'
let Tlist_Show_One_File = 1     "只显示当前文件的taglist，默认是显示多个
let Tlist_Exit_OnlyWindow = 1   "如果taglist是最后一个窗口，则退出vim
let Tlist_Use_Right_Window = 1  "在右侧窗口中显示taglist
let Tlist_GainFocus_On_ToggleOpen = 1       "光标保留在taglist窗口
let Tlist_Ctags_Cmd='/usr/bin/ctags'  "设置ctags命令的位置
nnoremap <leader>tl : Tlist<CR>     


""""""""""""""""""""""""""""""ariline配置"""""""""""""""""""""""""""
Bundle 'bling/vim-airline'
let g:airline#extensions#tabline#enabled = 1
if !exists('g:airline_symbols')
    let g:airline_symbols = {}
endif
"let g:airline_left_sep = '▶'
"let g:airline_left_alt_sep = '❯'
"let g:airline_right_sep = '◀'
"let g:airline_right_alt_sep = '❮'
"let g:airline_symbols.linenr = '¶'
"let g:airline_symbols.branch = '⎇'


""""""""""""""""""""""""""""syntastic配置"""""""""""""""""""""""
Bundle 'scrooloose/syntastic'
let g:syntastic_auto_loc_list = 1
"################### 快速导航 ###################
""目录导航
Bundle 'scrooloose/nerdtree'
map <leader>n :NERDTreeToggle<CR>
let NERDTreeHighlightCursorline=1
let NERDTreeIgnore=[ '\.pyc$', '\.pyo$', '\.obj$', '\.o$', '\.so$', '\.egg$', '^\.git$', '^\.svn$', '^\.hg$' ]
"let NERDTreeDirArrows=0
""let g:netrw_home='~/bak'
"close vim if the only window left open is a NERDTree
"autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTreeType") &&
"b:NERDTreeType == "primary") | q | end
"" s/v 分屏打开文件
let g:NERDTreeMapOpenSplit = 's'
let g:NERDTreeMapOpenVSplit = 'v'


Bundle 'jistr/vim-nerdtree-tabs'
map <Leader>n <plug>NERDTreeTabsToggle<CR>
" 关闭同步
let g:nerdtree_tabs_synchronize_view=0
let g:nerdtree_tabs_synchronize_focus=0
"自动开启nerdtree
"let g:nerdtree_tabs_open_on_console_startup=1


" Vim Workspace Controller
Bundle "szw/vim-ctrlspace"
let g:airline_exclude_preview = 1
"hi CtrlSpaceSelected guifg=#586e75 guibg=#eee8d5 guisp=#839496
"gui=reverse,bold ctermfg=10 ctermbg=7 cterm=reverse,bold
"hi CtrlSpaceNormal   guifg=#839496 guibg=#021B25 guisp=#839496 gui=NONE
"ctermfg=12 ctermbg=0 cterm=NONE
"hi CtrlSpaceSearch   guifg=#cb4b16 guibg=NONE gui=bold ctermfg=9
"ctermbg=NONE term=bold cterm=bold
"hi CtrlSpaceStatus   guifg=#839496 guibg=#002b36 gui=reverse term=reverse
"cterm=reverse ctermfg=12 ctermbg=8

" 标签导航
Bundle 'majutsushi/tagbar'
nmap <F9> :TagbarToggle<CR>
let g:tagbar_autofocus = 1

" golang 配置
Bundle 'fatih/vim-go'


